﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacroCaller
{
    public partial class DeveloperInfoForm : Form
    {
        public DeveloperInfoForm()
        {
            InitializeComponent();
            DeveloperInfoWindowProperties();
            DeveloperInfoSet();
        }

        public void DeveloperInfoSet()
        {
            devLabel1.Text =
                "Company: DS Skanding" +
                "\nDeveloper: Matija Bensa " +
                "\nYear: 2017";


        }

        private void DeveloperInfoWindowProperties()
        {
            this.ClientSize = new System.Drawing.Size(180, 155);
        }

        private void btn_devInfoOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
