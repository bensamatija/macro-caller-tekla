﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
//using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
//using TS = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;

namespace MacroCaller
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // Auto Load Settings:
            GetSetMacroDirectory();
            LoadFromXML(settingsFolderFullPath + "\\" + buttonPropertiesXML);
            this.Text = appName;
            StopDebugButtons();
            //SetKeyboardHook();    // Used for keyboard shortcuts
        }

        string appName = "MacroCaller";
        public string _XS_MACRO_DIRECTORY = "";
        string settingsFolderFullPath = "";
        public string macrosmodeling = "modeling";
        string macrodrawings = "drawings";
        //static string settingsFolder = "modeling\\MacroCaller_Settings";
        static string settingsFolder = "modeling\\MacroCaller\\MacroCaller_Settings";
        string buttonPropertiesXML = "MacroCaller_Buttons.xml";
        MyUserSettings myUserSettings;
        KeyboardHook kbh = new KeyboardHook();

        /// <summary>
        /// Gets the directory our Tekla is looking for macros
        /// Advanced options
        /// </summary>
        public void GetSetMacroDirectory()
        {
            try
            {
                T.TeklaStructuresSettings.GetAdvancedOption("XS_MACRO_DIRECTORY", ref _XS_MACRO_DIRECTORY);
                settingsFolderFullPath = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, settingsFolder));
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + "\n\nXS_MACRO_DIRECTORY not set properly,\nor Tekla Structures not running");
                ExitApplication();
            }
        }
        public static List<string> PropertyFileDirectories { get; set; }

        public int winW = 250, winH = 40;
        public int maxRows = 1, maxCols = 4;
        ToolTip myToolTip = new ToolTip();
        ToolTip ToolTip1 = new ToolTip();

        //public Button dynamicButton = new Button();
        public ButtonProperties b = new ButtonProperties();
        [XmlArray("ButtonsList")]
        List<ButtonProperties> buttonsList = new List<ButtonProperties>();
        [XmlArray("ButtonsListLoaded")]
        List<ButtonProperties> loadedList = new List<ButtonProperties>();

        private void StopDebugButtons()
        {
            button2.Hide();
            button3.Hide();
            btn_RunMacro.Hide();
        }
        /// <summary>
        /// Create beam button
        /// This can be removed later
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    TSM.Model myModel = new TSM.Model();
        //    TSM.Beam myBeam = new TSM.Beam(new TS.Point(1000, 1000, 1000), new TS.Point(6000, 6000, 1000));
        //    myBeam.Material.MaterialString = "S235JR";
        //    myBeam.Profile.ProfileString = "HEA400";
        //    myBeam.Insert();
        //    myModel.CommitChanges();
        //}

        /// <summary>
        /// Button Run Macro
        /// This can be removed later
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_RunMacro_Click(object sender, EventArgs e)
        {
            // Run modeling macro:
            string macroName = "Representation_standard.cs";
            RunThisMacro(macroName);
        }

        /// <summary>
        /// Set up some buttons and save to xml
        /// This can be removed later
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            // Create List of Button properties:
            buttonsList = new List<ButtonProperties>();

            // Create Buttons and Save them:

            b = new ButtonProperties();
            b.buttonName = "Button_1";
            b.macro = "DirectoryBrowser.cs";
            b.image = "default.jpg";
            b.row = 1;
            b.col = 1;
            buttonsList.Add(b);

            b = new ButtonProperties();
            b.buttonName = "Button_2";
            b.macro = "SKA_CreateSurfaceView.cs";
            b.image = "default.jpg";
            //string a = GetNameForImage(b.macro);         
            b.row = 1;
            b.col = 2;
            buttonsList.Add(b);

            b = new ButtonProperties();
            b.buttonName = "Button_3";
            b.macro = "SKA_InquireWeldMainPart.cs";
            b.image = "Capture.jpg";
            b.row = 2;
            b.col = 1;
            buttonsList.Add(b);

            b = new ButtonProperties();
            b.buttonName = "Button_4";
            b.macro = "SKA_RotateView_LeftSide.cs";
            b.image = "default.jpg";
            b.row = 2;
            b.col = 2;
            buttonsList.Add(b);

            // Save New Button Properties to XML file:
            SaveToXML(settingsFolderFullPath + "\\" + buttonPropertiesXML, buttonsList);
        }

        /// <summary>
        /// Set up each button
        /// </summary>
        /// <param name="btn"></param>
        private void CreateDynamicButton(ButtonProperties btn)
        {
            Button dynamicButton = new Button();

            //dynamicButton.Text = btn.macro.ToString().Remove(btn.macro.Length - 3);     // Predict that we have *.cs files only !
            dynamicButton.Name = btn.macro;
            dynamicButton.Height = 40;
            dynamicButton.Width = 40;
            string settingsFolderFullPath = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, settingsFolder));
            dynamicButton.Image = Image.FromFile(settingsFolderFullPath + "\\" + btn.image);
            dynamicButton.Image = ResizeImage(dynamicButton.Image, new Size(dynamicButton.Width, dynamicButton.Height));
            btn.pX = btn.col * dynamicButton.Width - dynamicButton.Width;
            btn.pY = btn.row * dynamicButton.Height - dynamicButton.Height;

            dynamicButton.Location = new Point(btn.pX, btn.pY);
            //dynamicButton.Font = new Font("Consolas", 8);

            dynamicButton.MouseEnter += new System.EventHandler(Hover);
            dynamicButton.Click += new EventHandler(DynamicButton_Click);
            Controls.Add(dynamicButton);

            // Form Window Size:
            FormResizer(btn, dynamicButton);
        }

        /// <summary>
        /// Resize the form based on the dynamic button's positions
        /// </summary>
        /// <param name="btn"></param>
        /// <param name="dynamicButton"></param>
        private void FormResizer(ButtonProperties btn, Button dynamicButton)
        {
            if (btn.row > maxRows)
            {
                maxRows = btn.row;
                winH = btn.row * dynamicButton.Height;
                this.ClientSize = new System.Drawing.Size(winW, winH + 15);   // 15 => space for statusText
            }
            if (btn.col > maxCols)
            {
                maxCols = btn.col;
                winW = btn.col * dynamicButton.Width;
                this.ClientSize = new System.Drawing.Size(winW, winH + 15);   // 15 => space for statusText
            }

            // Position others:
            statusText.Location = new Point(0, winH);
            chk_onTop.Location = new Point(winW - 80, winH);
            Info.Location = new Point(winW - 55, winH);
        }

        /// <summary>
        /// Resize the button's image
        /// </summary>
        /// <param name="imgToResize"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private static Image ResizeImage(Image imgToResize, Size size)
        {
            //Image img = new Bitmap(imgToResize, size);
            //Bitmap bitmap = new Bitmap(imgToResize);
            //bitmap.SetResolution(imgToResize.HorizontalResolution, imgToResize.VerticalResolution);
            //using (var graphics = Graphics.FromImage(imgToResize))
            //{
            //    graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //}


            //return imgToResize;
            return (Image)(new Bitmap(imgToResize, size));
        }

        /// <summary>
        /// This is the Load Button
        /// We can remove this later
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            // This is for reseting the max values for every load:
            maxCols = 4;
            maxRows = 0;
            //// Load the properties from XML to the Object:
            string _XS_MACRO_DIRECTORY = "";
            T.TeklaStructuresSettings.GetAdvancedOption("XS_MACRO_DIRECTORY", ref _XS_MACRO_DIRECTORY);
            string settingsXMLFullPath = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, settingsFolder, buttonPropertiesXML));
            LoadFromXML(settingsXMLFullPath);
        }

        /// <summary>
        /// This is the event that happens after we click on dynamicly created button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DynamicButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Identify which button was clicked:
                Button button = sender as Button;

                Image tempImage = button.Image;

                // Temporary change this button:
                //button.Enabled = false;
                //button.Image = Image.FromFile(settingsFolder + "\\buttonClick.png");      // If using fix image location !
                //button.ForeColor = Color.DarkSeaGreen;
                this.Enabled = false;

                //foreach (Control control in this.Controls)
                //{
                //    if (control.GetType() == typeof(Button))
                //    {
                //        if (control.Name.EndsWith(".cs"))
                //        {
                //            control.Enabled = false;
                //        }
                //    }
                //}

                RunThisMacro(button.Name);

                // Dispose all click events that happened udring that time:
                Application.DoEvents();

                //button.Enabled = true;
                this.Enabled = true;
                //button.Image = tempImage;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Serializing buttons properties
        /// </summary>
        /// <param name="buttonPropertiesXML"></param>
        /// <param name="buttonsList"></param>
        private void SaveToXML(string buttonPropertiesXML, List<ButtonProperties> buttonsList)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<ButtonProperties>));
                StreamWriter sw = new StreamWriter(buttonPropertiesXML);
                xmlSerializer.Serialize(sw, buttonsList);
                sw.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// DeSerializing buttons properties
        /// </summary>
        /// <param name="buttonPropertiesXML"></param>
        private void LoadFromXML(string buttonPropertiesXML)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<ButtonProperties>));
                StreamReader sr = new StreamReader(buttonPropertiesXML);
                loadedList = (List<ButtonProperties>)xmlSerializer.Deserialize(sr);
                foreach (ButtonProperties buttonProperties in loadedList)
                {
                    CreateDynamicButton(buttonProperties);
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ExitApplication();
            }
        }

        /// <summary>
        /// Run selected macro
        /// </summary>
        /// <param name="macroName"></param>
        private void RunThisMacro(string macroName)
        {
            try
            {
                string selectedMacro = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, macrosmodeling, macroName));

                if (System.IO.File.Exists(selectedMacro))
                {
                    //string state = (MacroState.Active.Equals(false).ToString());
                    //if (state == "False")
                    //{
                    Tekla.Structures.Model.Operations.Operation.RunMacro(macroName);    // It automaticly goes to modeling directory. For Drawing macro do @"../drawings/"
                    //}
                    //else
                    //{
                    //    System.Windows.Forms.MessageBox.Show(macroName + "Macro " + selectedMacro, "is allready running", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    //}
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(macroName + " not found, application stopped!\n\nCheck the files in " + selectedMacro, "Tekla Structures", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Windows always on top checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chk_onTop_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_onTop.Checked.Equals(true))
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        /// <summary>
        /// Text that appears after we hover the button with mouse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hover(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            //print(btn.Name);
            statusText.Font = new Font("Consolas", 8); //("Microsoft Sans Serif");
            statusText.Text = btn.Name;
        }

        /// <summary>
        /// Event on Closing the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFormClosing(object sender, FormClosingEventArgs e)
        {
            // Settings created in Project>>Properties>>Settings:
            myUserSettings.WindowPosition = this.Location;
            myUserSettings.Save();
            kbh.Keyboard_Unhook();
        }

        /// <summary>
        /// Event on Loading the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFormLoad(object sender, EventArgs e)
        {
            try
            {
                // See MyUserSettings.cs for SetUp
                myUserSettings = new MyUserSettings();
                this.StartPosition = FormStartPosition.Manual;
                this.FormBorderStyle = FormBorderStyle.Fixed3D;

                //this.Location = myUserSettings.WindowPosition;
                this.DataBindings.Add(new Binding("Location", myUserSettings, "WindowPosition"));
            }
            //catch (Exception)
            //{
            //    string userName = System.Environment.UserName;
            //    MessageBox.Show("Welcome new user:\n" + userName);
            //}
            catch { }
        }

        /// <summary>
        /// Developer info form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Info_Click(object sender, EventArgs e)
        {
            DeveloperInfoForm devForm = new DeveloperInfoForm();
            devForm.Enabled = true;
            devForm.Show();
        }

        private void SetKeyboardHook()
        {
            KeyboardHook kbh = new KeyboardHook();
            kbh.Keyboard_Hook();
        }

        private void ExitApplication()
        {
            kbh.Keyboard_Unhook();
            Environment.Exit(1);
        }

        // Helpers:
        private void print(string text)
        {
            Console.WriteLine(text);
        }
    }
}
