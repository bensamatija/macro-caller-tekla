﻿namespace MacroCaller
{
    partial class DeveloperInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.devLabel1 = new System.Windows.Forms.Label();
            this.btn_devInfoOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // devLabel1
            // 
            this.devLabel1.AutoSize = true;
            this.devLabel1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devLabel1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.devLabel1.Location = new System.Drawing.Point(12, 9);
            this.devLabel1.Name = "devLabel1";
            this.devLabel1.Size = new System.Drawing.Size(61, 13);
            this.devLabel1.TabIndex = 0;
            this.devLabel1.Text = "devLabel1";
            // 
            // btn_devInfoOk
            // 
            this.btn_devInfoOk.BackColor = System.Drawing.SystemColors.Desktop;
            this.btn_devInfoOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_devInfoOk.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn_devInfoOk.Location = new System.Drawing.Point(60, 126);
            this.btn_devInfoOk.Name = "btn_devInfoOk";
            this.btn_devInfoOk.Size = new System.Drawing.Size(60, 23);
            this.btn_devInfoOk.TabIndex = 1;
            this.btn_devInfoOk.Text = "OK";
            this.btn_devInfoOk.UseVisualStyleBackColor = false;
            this.btn_devInfoOk.Click += new System.EventHandler(this.btn_devInfoOk_Click);
            // 
            // DeveloperInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(184, 161);
            this.ControlBox = false;
            this.Controls.Add(this.btn_devInfoOk);
            this.Controls.Add(this.devLabel1);
            this.Enabled = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DeveloperInfoForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Developer Info";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label devLabel1;
        private System.Windows.Forms.Button btn_devInfoOk;
    }
}