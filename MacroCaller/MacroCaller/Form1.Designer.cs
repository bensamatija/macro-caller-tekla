﻿namespace MacroCaller
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_RunMacro = new System.Windows.Forms.Button();
            this.statusText = new System.Windows.Forms.Label();
            this.chk_onTop = new System.Windows.Forms.CheckBox();
            this.Info = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(175, 163);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(175, 192);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Load";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_RunMacro
            // 
            this.btn_RunMacro.Location = new System.Drawing.Point(94, 192);
            this.btn_RunMacro.Name = "btn_RunMacro";
            this.btn_RunMacro.Size = new System.Drawing.Size(75, 23);
            this.btn_RunMacro.TabIndex = 3;
            this.btn_RunMacro.Text = "Run Macro";
            this.btn_RunMacro.UseVisualStyleBackColor = true;
            this.btn_RunMacro.Click += new System.EventHandler(this.btn_RunMacro_Click);
            // 
            // statusText
            // 
            this.statusText.AutoSize = true;
            this.statusText.Location = new System.Drawing.Point(12, 239);
            this.statusText.Name = "statusText";
            this.statusText.Size = new System.Drawing.Size(13, 13);
            this.statusText.TabIndex = 4;
            this.statusText.Text = "_";
            this.statusText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // chk_onTop
            // 
            this.chk_onTop.Checked = true;
            this.chk_onTop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_onTop.Font = new System.Drawing.Font("Consolas", 8F);
            this.chk_onTop.Location = new System.Drawing.Point(175, 239);
            this.chk_onTop.Name = "chk_onTop";
            this.chk_onTop.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk_onTop.Size = new System.Drawing.Size(80, 17);
            this.chk_onTop.TabIndex = 0;
            this.chk_onTop.Text = "Top";
            this.chk_onTop.UseVisualStyleBackColor = true;
            this.chk_onTop.CheckedChanged += new System.EventHandler(this.chk_onTop_CheckedChanged);
            // 
            // Info
            // 
            this.Info.AutoSize = true;
            this.Info.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Info.Location = new System.Drawing.Point(201, 240);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(13, 13);
            this.Info.TabIndex = 5;
            this.Info.Text = "?";
            this.Info.Click += new System.EventHandler(this.Info_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.Info);
            this.Controls.Add(this.chk_onTop);
            this.Controls.Add(this.statusText);
            this.Controls.Add(this.btn_RunMacro);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form1";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormClosing);
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_RunMacro;
        private System.Windows.Forms.Label statusText;
        private System.Windows.Forms.CheckBox chk_onTop;
        private System.Windows.Forms.Label Info;
    }
}

